# frozen_string_literal: true

name 'rvm_io'
maintainer 'Hydrana SAS'
maintainer_email 'contact at hydrana dot io'
license 'MIT'
description 'Installs/Configures RVM, and intalls Rubies'
version '0.2.2'
chef_version '>= 16.0'

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
issues_url 'https://gitlab.com/hydrana/rvm.io-cookbook/-/issues'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
source_url 'https://gitlab.com/hydrana/rvm.io-cookbook'

depends 'apt'

supports 'ubuntu'
