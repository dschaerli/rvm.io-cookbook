# frozen_string_literal: true

# Chef InSpec test for recipe rvm_io::default

# The Chef InSpec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec/resources/

describe command('gpg --list-keys') do
  its('exit_status') { should eq 0 }
  its('stdout') { should match(/\[ultimate\] Michal Papis/) }
end

describe command('gpg --list-keys') do
  its('exit_status') { should eq 0 }
  its('stdout') { should match(/\[ultimate\] Piotr Kuczynski/) }
end

describe file('/usr/local/rvm/scripts/rvm') do
  it { should exist }
end

describe command("bash -c 'source /usr/local/rvm/scripts/rvm; rvm version'") do
  its('exit_status') { should eq 0 }
  its('stdout') { should match(/rvm \d\.\d+/) }
end

describe user('hydrana') do
  it { should exist }
  its('groups') { should include 'rvm' }
end
