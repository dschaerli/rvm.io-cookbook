# frozen_string_literal: true

# Chef InSpec test for recipe rvm_io::default

# The Chef InSpec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec/resources/

describe file('/root/.gemrc') do
  it { should exist }
  its('content') do
    should match(%r{gem: --no-document})
  end
end
