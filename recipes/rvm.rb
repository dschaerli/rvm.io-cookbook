# frozen_string_literal: true

#
# Cookbook:: rvm_io
# Recipe:: default
#
# Copyright:: 2022, The Authors, All Rights Reserved.

include_recipe 'rvm_io::packages'
include_recipe 'rvm_io::install_rvm'
