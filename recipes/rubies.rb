# frozen_string_literal: true

#
# Cookbook:: rvm_io
# Recipe:: default
#
# Copyright:: 2022, The Authors, All Rights Reserved.

#
# Per user RVM install
#

node['rvm_io']['users'].each do |username, rvm|
  next unless rvm['rubies']

  rvm['rubies'].each do |version, action|
    execute "installs Ruby #{version} for #{username} user" do
      command "bash -c 'source #{Etc.getpwnam(username).dir}/.rvm/scripts/rvm; rvm #{action} #{version}'"
      login true
      user username

      only_if { ::File.exist?("#{Etc.getpwnam(username).dir}/.rvm/scripts/rvm") }
      only_if do
        `sudo --user #{username} bash -c 'source #{Etc.getpwnam(username).dir}/.rvm/scripts/rvm; rvm list | grep ruby-#{version} | wc -l'` == "0\n"
      end
    end
  end
end

#
# System wide RVM install
#

Hash(node['rvm_io']['rubies']).each do |version, action|
  next if version == 'for'

  execute "installs Ruby #{version} as system wide" do
    command "bash -c 'source /usr/local/rvm/scripts/rvm; rvm #{action} #{version}'"
    login true
    user 'root'

    only_if { ::File.exist?('/usr/local/rvm/scripts/rvm') }
    only_if do
      `bash -c 'source /usr/local/rvm/scripts/rvm; rvm list | grep ruby-#{version} | wc -l'` == "0\n"
    end
  end
end
