# frozen_string_literal: true

default['rvm_io']['packages'] = value_for_platform(
  # rhel: { default: %w{} },
  # ubuntu: { default: %w{} },
  default: %w[bash]
)

default['rvm_io']['users'] = {}
